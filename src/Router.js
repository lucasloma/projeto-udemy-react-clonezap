
import { createStackNavigator, createMaterialTopTabNavigator } from 'react-navigation';
import LoginForm from './componets/LoginForm';
import CadastroForm from './componets/CadastroForm';
import Contatos from './componets/Contatos';
import AdicionarContato from './componets/AdicionarContato';
import Conversas from './componets/Conversas';
import headerPrincipal from './componets/headerPrincipal';
import Conversa from './componets/Conversa';


const topTab = createMaterialTopTabNavigator({
    'Contatos': {
        screen: Contatos
    },
    'Conversas': {
        screen: Conversas
    },
}, {
        tabBarPosition: 'top',
        tabBarOptions: {
            labelStyle: {
                fontSize: 16,
            },
            tabStyle: {
            },
            style: {
                backgroundColor: '#115E54',
            },
        }
    }
);

const App = createStackNavigator({
    'loginForm': {
        screen: LoginForm,
    },
    'contatos': {
        screen: topTab,
        navigationOptions: () => ({
            header: headerPrincipal
        })
    },
    'cadastro': {
        screen: CadastroForm,
    },
    'conversa': {
        screen: Conversa,
        navigationOptions: (navigation) => {
            const { contatoNome } = navigation.navigation.state.params;
            return {
                title: contatoNome,
                headerStyle: {
                    backgroundColor: '#115E54'
                },
                headerTitleStyle: {
                    color: 'white',
                }
            }
        }
    },
    'adicionarContato': {
        screen: AdicionarContato,
        navigationOptions: () => ({
            title: 'Novo Contato',
            headerStyle: {
                backgroundColor: '#115E54'
            },
            headerTitleStyle: {
                color: 'white',
            }
        }),
    }
},
    {
        navigationOptions: {
            headerTransparent: true,
            gesturesEnabled: true ///poder usar gestos para descartar essa tela.
        },
    }
);

export default App;