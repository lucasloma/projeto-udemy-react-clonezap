import firebase from 'firebase';
import b64 from 'base-64';
import _ from 'lodash'; ///transforma objeto em array


export const SET_FIELD = 'SET_FIELD'
export const setField = (field, value) => ({
    type: SET_FIELD,
    field,
    value
});

export const ADD_SUCESSO = 'ADD_SUCESSO';

export const ADD_ERROR = 'ADD_ERROR';
const addError = () => {
    return {
        type: ADD_ERROR,
        payload: 'Usuário não  encontrado'
    }
}
export const addContato = (email, navigation) => {
    let emailb64 = b64.encode(email);
    return dispatch => {
        firebase.database().ref(`/contatos/${emailb64}`)
            .once('value')
            .then(snapshot => {
                if (snapshot.val()) {
                    const dadosUsuario = _.first(_.values(snapshot.val()));///retorna dados usuário em array pega o primeiro no caso first
                    const { currentUser } = firebase.auth();
                    let emailUserb64 = b64.encode(currentUser.email);
                    firebase.database().ref(`/users_contatos/${emailUserb64}`).
                        push({ email, nome: dadosUsuario.nome })
                        .then(() => {
                            dispatch({ type: ADD_SUCESSO });
                            navigation.navigate('contatos');
                        })
                        .catch(error => console.log(error));
                } else {
                    dispatch(addError());
                }
            });
    }
};


export const LISTA_CONTATOS = 'LISTA_CONTATOS';
export const watchListaContatos = () => {
    const { currentUser } = firebase.auth();
    return dispatch => {
        let emailUserb64 = b64.encode(currentUser.email);
        firebase.database().ref(`/users_contatos/${emailUserb64}`)
            .on('value', snapshot => {
                const contatos = snapshot.val();
                dispatch({ type: LISTA_CONTATOS, payload: contatos })
            })
    }
};

export const MESG_SEND_SUCESS = 'MESG_SEND_SUCESS'
export const enviaMensagem = (mensagem, contatoEmail, contatoNome) => {
    ///Dados Usuario
    const { currentUser } = firebase.auth();
    const usuarioEmail = currentUser.email;

    return dispatch => {
        ///converter bs4
        const emailUsuariobs4 = b64.encode(usuarioEmail);
        const emailContatobs4 = b64.encode(contatoEmail);

        ///criação path mensagem entre os usuários  sendo armezenado em ambos os caminhos
        firebase.database().ref(`/mensagens/${emailUsuariobs4}/${emailContatobs4}`)
            .push({ mensagem, tipo: 'enviado' })
            .then(() => {
                firebase.database().ref(`/mensagens/${emailContatobs4}/${emailUsuariobs4}`)
                    .push({ mensagem, tipo: 'recebido' })
                    .then(() => dispatch({ type: MESG_SEND_SUCESS }))
            })
            .then(() => { /// Armazenar o cabeçalho da conversa  ao usuário auntenticado
                firebase.database().ref(`/usuario_conversas/${emailUsuariobs4}/${emailContatobs4}`)
                    .set({ nome: contatoNome, email: contatoEmail });
            })
            .then(() => {/// Armazena o cabeçalho da conversa ao contato
                ///resgatar nome do usuário conectado e adiciona o cabeçalho da conversa ao contato
                firebase.database().ref(`/contatos/${emailUsuariobs4}`)
                    .once('value')
                    .then(snapshot => {
                        const dadosUsuario = _.first(_.values(snapshot.val()));///retorna dados usuário em array pega o primeiro no caso first
                        firebase.database().ref(`/usuario_conversas/${emailContatobs4}/${emailUsuariobs4}`)
                            .set({ nome: dadosUsuario.nome, email: usuarioEmail });
                    })
            })
    }
}

export const LISTA_CONVERSA = 'LISTA_CONVERSA';
export const watchListaConversa = emailContato => {
    const { currentUser } = firebase.auth();
    return dispatch => {
        let emailUserb64 = b64.encode(currentUser.email);
        let emailContatobs4 = b64.encode(emailContato);
        firebase.database().ref(`/mensagens/${emailUserb64}/${emailContatobs4}`)
            .on('value', snapshot => {
                const conversa = snapshot.val();
                dispatch({ type: LISTA_CONVERSA, payload: conversa })
            })
    }
};

export const LISTA_CONVERSAS = 'LISTA_CONVERSAS';
export const watchListaConversas = () => {
    const { currentUser } = firebase.auth();
    return dispatch => {
        let emailUserb64 = b64.encode(currentUser.email);
        firebase.database().ref(`/usuario_conversas/${emailUserb64}`)
        .on('value', snapshot =>{
            dispatch({type:LISTA_CONVERSAS, payload:snapshot.val()})
        })

    }
}