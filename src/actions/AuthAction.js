import firebase from 'firebase';
import b64 from 'base-64';

export const SET_FIELD = 'SET_FIELD'
export const setField = (field, value) => ({
    type: SET_FIELD,
    field,
    value
});

export const tryLogin = (email, senha, navigation) => {
    return dispatch => {
        dispatch(isLonding());
        firebase.auth().signInWithEmailAndPassword(email, senha)
            .then(value => loginSucesso(dispatch, navigation))
            .catch(error => loginError(dispatch, error));
    }
};

export const LOGIN_SUCESSO = 'LOGIN_SUCESSO';
const loginSucesso = (dispatch, navigation) => {
    dispatch({ type: LOGIN_SUCESSO });
    navigation.replace('contatos');
};

export const LOGIN_ERROR = 'LOGIN_ERROR';
const loginError = (dispatch, error) => {
    dispatch({ type: LOGIN_ERROR, payload: error.message });
}

export const IS_LONDING = 'IS_LONDING';
const isLonding = () => {
    return {
        type: IS_LONDING,
        payload: true
    }
}

export const cadastarUsuario = (usuario, navigation) => {
    const { email, senha, nome } = usuario;
    return dispatch => {
        dispatch(isLonding());
        firebase.auth().createUserWithEmailAndPassword(email, senha)
            .then(user => {
                let email64 = b64.encode(email);/// converte em bs4 para salavar o email junto as contatos devido ao firebase nao permitir salvar carateres especiais como id ex: @.
                firebase.database().ref(`/contatos/${email64}`).push({ nome }) ///Tomar cuidado com espaços porque serão salvos
                    .then(value => cadastroUsuarioSucesso(dispatch, navigation))
            })
            .catch(error => cadastroUsuarioErro(error, dispatch));
    }
};

export const CADASTRADO_SUCESSO = 'CADASTRO_SUCESSO';
const cadastroUsuarioSucesso = (dispatch, navigation) => {
    dispatch({ type: CADASTRADO_SUCESSO });
    navigation.navigate('main');
}

export const CADASTRO_ERRO = 'CADASTRO_ERRO';
const cadastroUsuarioErro = (error, dispatch) => {
    dispatch({ type: CADASTRO_ERRO, payload: error.message });
}