import { SET_FIELD, ADD_ERROR, ADD_SUCESSO,MESG_SEND_SUCESS } from '../actions/AppActions'

INITIAL_STATE = {
    adicionar_contato_email: '',
    add_Error: '',
    mensagem:''
};

export default function appReducers(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_FIELD:
            const newSatate = { ...state, add_Error: '' };
            newSatate[action.field] = action.value;
            return newSatate;
        case ADD_ERROR:
            return { ...state, add_Error: action.payload }
        case ADD_SUCESSO:
            return {...state, adicionar_contato_email:''};
            case MESG_SEND_SUCESS:
            return {...state,mensagem:''}
        default:
            return state;
    }
};