import {LISTA_CONVERSAS} from '../actions/AppActions'

const INITIAL_STATE ={}

export default function ConversasReducers (state = INITIAL_STATE,action){
    switch(action.type){
        case LISTA_CONVERSAS:
        return action.payload
        default:
        return state;
    }
}