import { SET_FIELD, CADASTRO_ERRO, LOGIN_ERROR, LOGIN_SUCESSO, CADASTRADO_SUCESSO, IS_LONDING } from '../actions/AuthAction'

const INITIAL_STATE = {
    nome: '',
    email: '',
    senha: '',
    mensagemErro: '',
    erroLogin: '',
    isLonding: false
}

export default function authReducers(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_FIELD:
            const newSatate = { ...state };
            newSatate[action.field] = action.value;
            return newSatate
        case CADASTRO_ERRO:
            return { ...state, mensagemErro: action.payload, isLonding:false }
        case CADASTRADO_SUCESSO:
            return { ...state, nome: '', senha: '', isLonding: false }
        case LOGIN_ERROR:
            return { ...state, erroLogin: action.payload, isLonding: false }
        case LOGIN_SUCESSO:
            return {...state, ...INITIAL_STATE} ///voltara ao estado inicial as variaveis do auth para quando for sair já está limpo os campos
        case IS_LONDING:
            return { ...state, isLonding: action.payload }
        default:
            return state;
    }
}