import {LISTA_CONVERSA} from '../actions/AppActions'

const INITIAL_STATE ={}

export default function ConversaReducers (state = INITIAL_STATE,action){
    switch(action.type){
        case LISTA_CONVERSA:
        return action.payload
        default:
        return state;
    }
}
