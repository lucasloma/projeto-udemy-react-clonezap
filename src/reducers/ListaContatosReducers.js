import { LISTA_CONTATOS } from '../actions/AppActions';

INITIAL_STATE = {}

export default function ContatosReducers(state = INITIAL_STATE, action) {
    switch (action.type) {
        case LISTA_CONTATOS:
            return action.payload;
        default:
            return state;
    }

}