import {combineReducers} from 'redux';
import authReducers from './AuthReducers';
import appReducers from './AppReducers'
import ContatosReducers from './ListaContatosReducers'
import ConversaReducers from './ListaConversaReducers'
import ConversasReducers from './ListaConversasReducers'


export default combineReducers({
  authReducers,
  appReducers,
  ContatosReducers,
  ConversaReducers,
  ConversasReducers
});