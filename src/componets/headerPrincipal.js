import React from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native'
import firebase from 'firebase'

const AddContato = require('../../assets/adicionar-contato.png');

const headerPrincipal = (props) => (
    <View style={{
        height: 90
        , backgroundColor: '#115E54'
        , justifyContent: 'space-between',
        alignItems: "center",
        flexDirection: 'row',
        elevation: 4
    }}>
        <Text style={{ color: '#FFF', fontSize: 18, margin: 10 }}>ZAP CLONE</Text>
        <View style={{
            justifyContent: 'space-between',
            alignItems: "center",
            flexDirection: 'row',
        }}>
            <TouchableOpacity style={{ margin: 20 }}
                onPress={() => props.navigation.navigate('adicionarContato')}>
                <Image
                    source={AddContato} />
            </TouchableOpacity>
            <View>
                <TouchableOpacity onPress={()=> firebase.auth().signOut()
                .then(() => (props.navigation.navigate('loginForm')))}>
                    <Text style={{ color: '#FFF', fontSize: 18, margin: 10 }}>Sair</Text>
                </TouchableOpacity>
            </View>
        </View>
    </View>
);

export default headerPrincipal;