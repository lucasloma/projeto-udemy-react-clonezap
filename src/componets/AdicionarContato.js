import React from 'react';
import { View, Text, Button, StyleSheet, TextInput } from 'react-native';
import { connect } from 'react-redux'
import { setField, addContato } from '../actions/AppActions'

const AdicionarContatos = (props) => (
    <View style={styles.container}>
        <View style={styles.containerInput}>
            <TextInput value={props.adicinarEmail}
                style={styles.inputTexto} placeholder='E-mail'
                onChangeText={texto => props.setField('adicionar_contato_email', texto)} />
        </View>
        <View style={styles.containerBotao}>
            <Button title='Adicionar'
                color="#115E54"
                onPress={() => props.addContato(props.adicinarEmail, props.navigation)} />
            <Text style={styles.textoErro}>{props.add_Error}</Text>
        </View>
    </View>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',

    },
    containerInput: {
        marginTop: 90
    },
    inputTexto: {
        fontSize: 20,
        height: 45,
    },
    containerBotao: {
        marginTop: 30
    },
    textoErro: {
        color: 'red',
        fontSize: 20
    }
});

const mapStateToProps = state => {
    const { adicionar_contato_email, add_Error } = state.appReducers
    return {
        adicinarEmail: adicionar_contato_email,
        add_Error
    }
};

const mapDispatchToProps = {
    setField,
    addContato
};

export default connect(mapStateToProps, mapDispatchToProps)(AdicionarContatos);