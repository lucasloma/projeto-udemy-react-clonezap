import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, ImageBackground, ActivityIndicator } from 'react-native';

import { connect } from 'react-redux';
import { setField, cadastarUsuario } from '../actions/AuthAction'

class CadastroForm extends React.Component {
    render() {
        const { authReducers, navigation } = this.props
        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/bg.png')}>
                <View style={styles.container}>
                    <View style={styles.containerTexto}>
                        <Text style={styles.titulo}>Cadastrar</Text>
                    </View>
                    <View style={styles.containerInput}>
                        <TextInput value={authReducers.nome}
                            style={styles.inputTexto} placeholder='Nome'
                            onChangeText={texto => this.props.setField('nome', texto)} />
                        <TextInput value={authReducers.email}
                            style={styles.inputTexto} placeholder='E-mail'
                            onChangeText={texto => this.props.setField('email', texto)} />
                        <TextInput value={authReducers.senha}
                            style={styles.inputTexto} placeholder='senha'
                            onChangeText={texto => this.props.setField('senha', texto)} />
                        <Text style={styles.textoErro}>{authReducers.mensagemErro}</Text>
                    </View>
                    <View style={styles.containerBotao}>
                        {this.renderButton()}
                    </View>
                </View>
            </ImageBackground>
        );
    }

    renderButton() {
        const { isLonding } = this.props.authReducers;
        if (isLonding) {
            return <ActivityIndicator size='large' />
        }
        return (
            <Button title='Cadastrar'
                color="#115E54"
                onPress={() => this._cadastrarUsuario()} />
        );
    }

    _cadastrarUsuario() {
        this.props.cadastarUsuario(this.props.authReducers, this.props.navigation)
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    containerTexto: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    titulo: {
        fontSize: 25,
        color: '#FFF',
        fontWeight: 'bold',
    },
    containerInput: {
        flex: 2,
    },
    inputTexto: {
        fontSize: 20,
        height: 45,
    },
    containerBotao: {
        flex: 2
    },
    textoErro: {
        color: 'red',
        fontSize: 20
    }
});

mapStateToProps = state => {
    return {
        authReducers: state.authReducers
    }
}

const mapDispatchToProps = {
    setField,
    cadastarUsuario
};

export default connect(mapStateToProps, mapDispatchToProps)(CadastroForm);