import React from 'react';
import {
    View,
    Text,
    TextInput,
    Image,
    TouchableHighlight,
    StyleSheet,
    KeyboardAvoidingView,
    ListView
} from 'react-native';
import { connect } from 'react-redux';
import { setField, enviaMensagem, watchListaConversa } from '../actions/AppActions'
import _ from 'lodash'; ///transforma objeto em array

class Conversa extends React.Component {

    componentWillMount() {
        const { contatoEmail } = this.props.navigation.state.params;
        this.props.watchListaConversa(contatoEmail);
        this.criaFonteDados(this.props.conversa);
    }

    ///este método e executado depois da montagem no componente ou quando alterado uma props
    componentWillReceiveProps(nextProps) {
        this.criaFonteDados(nextProps.conversa);
    }

    criaFonteDados(conversa) {
        ///Preparação do dataSource do LisView onde vai tratar os dados nas linhas 
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
        this.fonteDados = ds.cloneWithRows(conversa)
    }

    renderRow(conversa) {

        if (conversa.tipo == 'enviado') {
            return (
                <View style={{ alignItems: 'flex-end', marginTop: 5, marginBottom: 5, marginLeft: 40 }}>
                    <Text style={{ fontSize: 18, color: '#000', padding: 10, backgroundColor: '#dbf5b4', elevation: 1 }}>{conversa.mensagem}</Text>
                </View>
            )
        }
        return (
            <View style={{ alignItems: 'flex-start', marginTop: 5, marginBottom: 5, marginRight: 40 }}>
                <Text style={{ fontSize: 18, color: '#000', padding: 10, backgroundColor: '#f7f7f7', elevation: 1 }}>{conversa.mensagem}</Text>
            </View>
        );
    }

    render() {
        const { contatoEmail, contatoNome } = this.props.navigation.state.params;
        return (
            <View style={style.containar}>
                <View style={style.containarListMensagem} >
                    <ListView
                        enableEmptySections /// para para mensagem de erro do header
                        dataSource={this.fonteDados}
                        renderRow={data => this.renderRow(data)}
                    />
                </View>
                <KeyboardAvoidingView behavior="padding" enabled keyboardVerticalOffset={50}>
                    <View style={style.containarEnviaMensagem}>
                        <TextInput
                            value={this.props.mensagem}
                            onChangeText={(texto) => this.props.setField('mensagem', texto)}
                            style={style.textInput} placeholder='Digite aqui' ></TextInput>
                        <TouchableHighlight
                            onPress={() => this.props.enviaMensagem(this.props.mensagem, contatoEmail, contatoNome)}
                            underlayColor='#fff'>
                            <Image
                                source={require('../../assets/enviar_mensagem.png')}
                            />
                        </TouchableHighlight>
                    </View>
                </KeyboardAvoidingView>
            </View>
        )
    }
}

const style = StyleSheet.create({
    containar: {
        flex: 1,
        marginTop: 50,
        padding: 10,
        backgroundColor: '#eee4dc'
    },
    containarListMensagem: {
        flex: 1,
        paddingBottom: 20,
        marginTop:30

    },
    containarEnviaMensagem: {
        flexDirection: 'row',
        height: 60,
    },
    textInput: {
        flex: 4,
        backgroundColor: '#FFF',
        fontSize: 18
    }
})

const mapStateToProps = state => {
    const { mensagem } = state.appReducers
    const conversa = _.map(state.ConversaReducers, (val, uid) => {
        return { ...val, uid }
    });
    return {
        mensagem,
        conversa
    }
}

const mapDispatchToProps = {
    setField,
    enviaMensagem,
    watchListaConversa
}
export default connect(mapStateToProps, mapDispatchToProps)(Conversa);