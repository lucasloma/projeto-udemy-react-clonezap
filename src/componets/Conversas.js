import React from 'react';
import { View, Text, ListView, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { watchListaConversas } from '../actions/AppActions';
import _ from 'lodash';

class Conversas extends React.Component {
    componentWillMount() {
        this.props.watchListaConversas();
        this.criaFonteDados(this.props.conversas);
    }
    ///este método e executado depois da montagem no componente ou quando alterado uma props
    componentWillReceiveProps(nextProps) {
        this.criaFonteDados(nextProps.conversas);
    }

    criaFonteDados(conversas) {
        ///Preparação do dataSource do LisView onde vai tratar os dados nas linhas 
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
        this.fonteDados = ds.cloneWithRows(conversas)
    }

    renderRow(conversas) {
        return (
            <TouchableHighlight underlayColor='#fff' onPress={() => this.props.navigation.navigate('conversa', { contatoNome: conversas.nome, contatoEmail: conversas.email })}>
                <View style={{ flex: 1, padding: 20, borderBottomWidth: 1, borderColor: '#CCC' }}>
                    <Text style={{ fontSize: 25 }}>{conversas.nome}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <ListView
                enableEmptySections /// para para mensagem de erro do header
                dataSource={this.fonteDados}
                renderRow={data => this.renderRow(data)}
            />
        )
    }
}


const mapStateToProps = (state) => {
    const conversas = _.map(state.ConversasReducers, (val, uid) => {
        return { ...val, uid }
    });
    return {
        conversas
    }
}

const mapDispachToProps = {
    watchListaConversas
}
export default connect(mapStateToProps, mapDispachToProps)(Conversas);