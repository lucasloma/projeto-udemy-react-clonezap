import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, TouchableOpacity, ImageBackground, ActivityIndicator } from 'react-native';

import { connect } from 'react-redux';
import { setField, tryLogin } from '../actions/AuthAction'

class LoginForm extends React.Component {
    render() {
        const { authReducers, setField } = this.props;
        return (
            <ImageBackground style={{ flex: 1 }} source={require('../../assets/bg.png')}>
                <View style={styles.container}>
                    <View style={styles.containerTexto}>
                        <Text style={styles.titulo}>ZapZap Clone </Text>
                    </View>
                    <View style={styles.containerInput}>
                        <TextInput value={authReducers.email}
                            style={styles.inputTexto} placeholder='E-mail'
                            onChangeText={texto => { setField('email', texto) }} />
                        <TextInput value={authReducers.senha}
                            secureTextEntry
                            style={styles.inputTexto} placeholder='Senha'
                            onChangeText={texto => { setField('senha', texto) }} />
                        <Text style={styles.textoErro}>{authReducers.erroLogin}</Text>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('cadastro')} >
                            <Text style={styles.textoCadastro}>Deseja se cadastar ?  Clique Aqui!!</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.containerBotao}>
                        {this.renderButton()}
                    </View>
                </View>
            </ImageBackground>
        );
    }

    _tryLogin() {
        const { email, senha } = this.props.authReducers;
        this.props.tryLogin(email, senha, this.props.navigation);
    };

    renderButton() {
        const { isLonding } = this.props.authReducers;
        if (isLonding) {
            return <ActivityIndicator size='large' />
        }
        return (
            <Button title='Entar'
                color="#115E54"
                onPress={() => { this._tryLogin()}} />
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    containerTexto: {
        flex: 1,
        justifyContent: 'center',
        alignItems: "center"
    },
    titulo: {
        fontSize: 28,
        color: '#FFF',
        fontWeight: 'bold',
    },
    containerInput: {
        flex: 2,
    },
    inputTexto: {
        fontSize: 20,
        height: 45,
    },
    textoCadastro: {
        fontSize: 20,
        color: '#FFF',
        fontWeight: 'bold',
    },
    containerBotao: {
        flex: 2
    },
    textoErro: {
        color: 'red',
        fontSize: 20
    }
});

mapStateToProps = state => {
    return {
        authReducers: state.authReducers
    }
}

const mapDispatchToProps = {
    setField,
    tryLogin
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);