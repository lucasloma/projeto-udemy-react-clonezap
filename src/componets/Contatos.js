import React from 'react';
import { View, Text, ListView, TouchableHighlight } from 'react-native';
import { connect } from 'react-redux';
import { watchListaContatos } from '../actions/AppActions'
import _ from 'lodash'; ///transforma objeto em array

class Contatos extends React.Component {

    componentWillMount() {
        this.props.watchListaContatos();
        this.criaFonteDados(this.props.contatos);
    }

    ///este método e executado depois da montagem no componente ou quando alterado uma props
    componentWillReceiveProps(nextProps) {
        this.criaFonteDados(nextProps.contatos);
    }

    criaFonteDados(contatos) {
        ///Preparação do dataSource do LisView onde vai tratar os dados nas linhas 
        const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })
        this.fonteDados = ds.cloneWithRows(contatos)
    }

    renderRow(contatos) {
        return (
            <TouchableHighlight underlayColor='#fff' onPress={() => this.props.navigation.navigate('conversa', { contatoNome: contatos.nome, contatoEmail:contatos.email })}>
                <View style={{ flex: 1, padding: 20, borderBottomWidth: 1, borderColor: '#CCC' }}>
                    <Text style={{ fontSize: 25 }}>{contatos.nome}</Text>
                    <Text style={{ fontSize: 20 }}>{contatos.email}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    render() {
        return (
            <ListView
                enableEmptySections /// para para mensagem de erro do header
                dataSource={this.fonteDados}
                renderRow={data => this.renderRow(data)}
            />
        );
    }
}

const mapStateToProps = (state) => {
    const contatos = _.map(state.ContatosReducers, (val, uid) => {
        return { ...val, uid }
    });
    return {
        contatos
    }
}

const mapDispachToProps = {
    watchListaContatos
}

export default connect(mapStateToProps, mapDispachToProps)(Contatos);