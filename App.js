import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
///Importação redux para tratamento dos state
import { Provider } from 'react-redux'; /// faz a ligação do react com redux
import { createStore, applyMiddleware } from 'redux';
/// Importação redux thunk para tratamentos das actions aynsc
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers' /// importa o combine redux onde estão gerenciados o redux
//Importação das rotas
import Router from './src/Router';
//importação firebase
import firebase from 'firebase';

const store = createStore(reducers ,{},applyMiddleware(ReduxThunk));
console.disableYellowBox = true; //// para desabilitar menssagem de erros de avisos.
export default class App extends React.Component {

  componentWillMount() {
    var config = {
      apiKey: "AIzaSyBzSWeCxfytcGbHKxz_0K8nAHI3cCIfT3s",
      authDomain: "zapzapclone-96492.firebaseapp.com",
      databaseURL: "https://zapzapclone-96492.firebaseio.com",
      projectId: "zapzapclone-96492",
      storageBucket: "zapzapclone-96492.appspot.com",
      messagingSenderId: "171714407681"
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
